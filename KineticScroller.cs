﻿using System;
using System.Diagnostics;
using System.Windows.Threading;

namespace KineticScroll
{
	/// <summary>
	/// Contains functionality for scrolling with momentum.
	/// </summary>
	internal sealed class KineticScroller
	{
		#region Events

		/// <summary>
		/// Raised when the value of Position has changed.
		/// </summary>
		public event EventHandler PositionChanged;

		#endregion Events

		#region Variables

		private bool _isUserScrolling;
		private readonly DispatcherTimer _timer;

		private double _refreshRate;
		private double _deceleration;
		private double _maxVelocity;
		private double _scrollRange;

		private double _position;
		public double TargetPosition;

		private double _velocity;

		#endregion Variables

		#region Properties

		/// <summary>
		/// Gets or sets the number of milliseconds between refreshing the scroll position.
		/// </summary>
		/// <value>The refresh interval.</value>
		public double RefreshRate
		{
			get
			{
				return _refreshRate;
			}
			set
			{
				_refreshRate = value;
				_timer.Interval = TimeSpan.FromMilliseconds(1000.0 / _refreshRate);
			}
		}

		/// <summary>
		/// Gets or sets the deceleration (in pixels per millisecond) to apply to scrolling each interval.
		/// </summary>
		/// <value>The deceleration.</value>
		public double Deceleration
		{
			get
			{
				return _deceleration;
			}
			set
			{
				_deceleration = value;
			}
		}

		/// <summary>
		/// Gets or sets the maximum scrolling velocity (in pixels per millisecond).
		/// </summary>
		/// <value>The max velocity.</value>
		public double MaxVelocity
		{
			get
			{
				return _maxVelocity;
			}
			set
			{
				_maxVelocity = value;
			}
		}

		/// <summary>
		/// Gets or sets the scroll range, e.g. the height of the page that will be scrolled.
		/// </summary>
		/// <value>The scroll range.</value>
		public double ScrollRange
		{
			get
			{
				return _scrollRange;
			}
			set
			{
				_scrollRange = value;
			}
		}

		/// <summary>
		/// Gets or sets the user's position, e.g. the position of the cursor.
		/// </summary>
		/// <value>The position.</value>
		public double Position
		{
			get
			{
				return _position;
			}
			set
			{
				if (_position != value)
				{
					_position = value;
					OnPositionChanged(EventArgs.Empty);
				}
			}
		}

		private double Velocity
		{
			get
			{
				return _velocity;
			}
			set
			{
				// If the value is more than the max then silently fix it
				if (Settings.Instance.ScrollSize == 0 && Math.Abs(value) > _maxVelocity)
				{
					_velocity = Math.Sign(value) * _maxVelocity;
				}
				else
				{
					_velocity = value;
				}
			}
		}

		#endregion Properties

		#region Constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="KineticScroller"/> class.
		/// </summary>
		/// <param name="refreshRate">The refresh interval.</param>
		/// <param name="deceleration">The deceleration.</param>
		/// <param name="maxVelocity">The max velocity.</param>
		/// <param name="scrollRange">The scroll range.</param>
		public KineticScroller(double refreshRate, double deceleration, double maxVelocity, double scrollRange)
		{
			_refreshRate = refreshRate;
			_deceleration = deceleration;
			_maxVelocity = maxVelocity;
			_scrollRange = scrollRange;

			_timer = new DispatcherTimer();
			_timer.Interval = TimeSpan.FromMilliseconds(1000.0 / _refreshRate);

			_timer.Tick += Timer_Tick;
			_timer.Start();
		}

		#endregion Constructor

		#region Public Methods

		public void StartUserScroll(double startPosition)
		{
			// Stop any previous scrolling
			//StopScrollAnimation();

			// Set the initial positions to where the user started scrolling
			_position = startPosition;
			TargetPosition = startPosition;

			// The user is scrolling
			_isUserScrolling = true;

			// Start the animation timer
			_timer.IsEnabled = true;
		}

		public void StopUserScroll()
		{
			// The user has stopped scrolling
			// We don't want to stop the animation timer though, because we will be decelerating
			_isUserScrolling = false;
		}

		public void UpdateUserScroll(double newPosition)
		{
			// The user has scrolled to this position
			// The actual position will be updated in the timer's Tick event handler
			TargetPosition = newPosition;
		}

		public void StopScrollAnimation()
		{
			// Stop the animation timer
			_timer.IsEnabled = false;
		}

		public void UpdateSmoothScroll(double offset)
		{
			TargetPosition += offset;

			_timer.IsEnabled = true;
		}

		public void ChangeVelocity(double velocityChange)
		{
			// Stop any previous scrolling
			//StopScrollAnimation();

			// If we're at a low velocity or changing direction, just set the velocity to 1
			if (/*Math.Abs(Velocity) < 1 ||*/ Math.Sign(Velocity) != Math.Sign(velocityChange))
			{
				Velocity = Math.Sign(velocityChange);
			}
			else
			{
				Velocity *= Math.Abs(velocityChange);
			}

			// And start the animation timer
			_timer.IsEnabled = true;
		}

		#endregion Public Methods

		#region Private Scroll Methods

		/// <summary>
		/// Handles the Tick event of the Timer control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		private void Timer_Tick(object sender, EventArgs e)
		{
			try
			{
				if (_isUserScrolling)
				{
					// Calculate the velocity from the distance the cursor has traveled in the last interval
					double displacement = TargetPosition - _position;
					Velocity = displacement / (1000.0 / _refreshRate);

					// The user is scrolling, so just go instantly to their point
					Position = TargetPosition;
				}
				else if (Settings.Instance.ScrollSize > 0)
				{
					double error = TargetPosition - _position;

					// The amount the position has changed is given by velocity x time
					Velocity = Math.Sign(error) * (Math.Pow(Math.Abs(error) + 1, 1.35) - 1) / (1000.0 / _refreshRate) / 20;

					var delta = _velocity * (1000.0 / _refreshRate);
					if (delta > 0)
						delta = Math.Max(.5, delta);
					else if (delta < 0)
						delta = Math.Min(-.5, delta);

					// if we passed target we are done
					double error2 = TargetPosition - (_position + delta);
					if (Math.Sign(error) != Math.Sign(error2) || error2 == 0)
					{
						Position = TargetPosition;
						StopScrollAnimation();
					}
					else
						Position += delta;
				}
				else
				{
					// Change the last sampled velocity by the deceleration
					double changeVelocity = _deceleration / (1000.0 / _refreshRate) / (1000.0 / _refreshRate);

					if (changeVelocity > Math.Abs(_velocity))
					{
						Velocity = 0;
						StopScrollAnimation();
					}
					else
					{
						Velocity -= Math.Sign(Velocity) * changeVelocity;
					}

					// The amount the position has changed is given by velocity x time
					Position += _velocity * (1000.0 / _refreshRate);
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine("Timer_Tick:  " + ex.Message);
			}
		}

		#endregion Private Scroll Methods

		#region Event Methods

		/// <summary>
		/// Raises the <see cref="E:PositionChanged"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		private void OnPositionChanged(EventArgs e)
		{
			if (PositionChanged != null)
				PositionChanged(this, e);
		}

		#endregion Event Methods
	}
}
