﻿This Version:
-------------

Test at various zooms
Mousing off the top or bottom when scrolling should keep scrolling and decelerate

Next Version:
-------------

Make KineticScrolller more WPF Bindable (INotifyPropertyChanged, set properties to start scrolling, etc)
Make an adorner with a grabbing hand for the cursor
Decelerate scrolling at the top and bottom to rest

Questions:
----------

How do I handle exceptions?
How do I auto-update?
