﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Kinetic Scroll")]
[assembly: AssemblyDescription("Adds kinetic scrolling to the Visual Studio text editor")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Andrew K, Philip Gruebele")]
[assembly: AssemblyProduct("Kinetic Scroll")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(false)]
[assembly: NeutralResourcesLanguage("en-US")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("1.1.0.0")]
[assembly: AssemblyFileVersion("1.1.0.0")]

[assembly: InternalsVisibleTo("KineticScroll_IntegrationTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100d780613d7210f4ef607170f5f87c3b68121d0c65c29fe09cfceb449627fe2013922183ec1bf3e9bcbea43fe409c2231d66a3bcf5720f964392c96e9cf4cd8a8af919e3f9524da2c2de2cc5d3ec0e6e6d620b51aa510f1f4a0142d3de3eb2e5208b00f4429906086f3a7feef80a9bc59b7ac3acece6a22d3cead722916345eed1")]
[assembly: InternalsVisibleTo("KineticScroll_UnitTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100d780613d7210f4ef607170f5f87c3b68121d0c65c29fe09cfceb449627fe2013922183ec1bf3e9bcbea43fe409c2231d66a3bcf5720f964392c96e9cf4cd8a8af919e3f9524da2c2de2cc5d3ec0e6e6d620b51aa510f1f4a0142d3de3eb2e5208b00f4429906086f3a7feef80a9bc59b7ac3acece6a22d3cead722916345eed1")]
