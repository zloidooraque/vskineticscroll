namespace KineticScroll
{
    /// <summary>
    /// Contains the constants used in this package.
    /// </summary>
    internal sealed class PackageConstants
    {
        /// <summary>
        /// The GUID for the package class.
        /// </summary>
        public const string PackageGuid = "f8971b4a-0c31-4c23-bb88-34615283c163";

        /// <summary>
        /// The GUID for the options page class.
        /// </summary>
        public const string OptionsPageGuid = "CBEBE463-71CB-4322-ACC2-C31DD2B26212";

        /// <summary>
        /// The resource ID for the extension name.
        /// </summary>
        public const string ProductNameResourceID = "#110";

        /// <summary>
        /// The resource ID for the extension description.
        /// </summary>
        public const string ProductDetailsResourceID = "#112";

        /// <summary>
        /// The extension version description.
        /// </summary>
        public const string ProductVersion = "1.2";

        /// <summary>
        /// The resource ID for the extension name.
        /// </summary>
        public const int ProductIconResourceID = 400;

        /// <summary>
        /// The resource ID for the options page category.
        /// </summary>
        public const string OptionsPageCategoryResourceID = "#114";

        /// <summary>
        /// The resource number for the options page category.
        /// </summary>
        public const int OptionsPageCategoryResourceNumber = 114;

        /// <summary>
        /// The resource ID for the options page name.
        /// </summary>
        public const string OptionsPageNameResourceID = "#116";

        /// <summary>
        /// The resource number for the options page name.
        /// </summary>
        public const int OptionsPageNameResourceNumber = 116;
    }
}