﻿using System;
using System.Diagnostics;
using System.Windows.Input;
using Microsoft.VisualStudio.Text.Editor;

namespace KineticScroll
{
	/// <summary>
	/// Overrides Visual Studio mouse bindings to provide kinetic scrolling.
	/// </summary>
	internal sealed class KineticScrollMouseProcessor : MouseProcessorBase
	{
		#region Variables

		private IWpfTextView _textView;

		private KineticScroller _scrollHelper;
		private double _currentPosition;

		#endregion Variables

		#region Constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="KineticScrollMouseProcessor"/> class.
		/// </summary>
		/// <param name="textView">The text view.</param>
		public KineticScrollMouseProcessor(IWpfTextView textView)
		{
			try
			{
				_textView = textView;

				// Initialize our scroll helper with the text view's height
				_scrollHelper = new KineticScroller(Settings.Instance.RefreshRate, Settings.Instance.Deceleration, Settings.Instance.MaxVelocity, _textView.ViewportHeight);

				// Listen for changes to settings and the text view closing so that we can unhook those listeners
				Settings.Instance.RefreshRateChanged += Settings_RefreshRateChanged;
				Settings.Instance.DecelerationChanged += Settings_DecelerationChanged;
				Settings.Instance.MaxVelocityChanged += Settings_MaxVelocityChanged;
				_textView.ViewportHeightChanged += TextView_ViewportHeightChanged;
				_textView.Closed += TextView_Closed;

				// Listen for changes to the scroll helper's Position property so that we can update the text view's scroll position
				_scrollHelper.PositionChanged += ScrollHelper_PositionChanged;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("KineticScrollMouseProcessor:  " + ex.Message);
			}
		}

		private void TextView_ViewportHeightChanged(object sender, EventArgs e)
		{
			_scrollHelper.ScrollRange = _textView.ViewportHeight;
		}

		private void Settings_RefreshRateChanged(object sender, EventArgs e)
		{
			_scrollHelper.RefreshRate = Settings.Instance.RefreshRate;
		}

		private void Settings_DecelerationChanged(object sender, EventArgs e)
		{
			_scrollHelper.Deceleration = Settings.Instance.Deceleration;
		}

		private void Settings_MaxVelocityChanged(object sender, EventArgs e)
		{
			_scrollHelper.MaxVelocity = Settings.Instance.MaxVelocity;
		}

		private void TextView_Closed(object sender, EventArgs e)
		{
			Settings.Instance.RefreshRateChanged -= Settings_RefreshRateChanged;
			Settings.Instance.DecelerationChanged -= Settings_DecelerationChanged;
			Settings.Instance.MaxVelocityChanged -= Settings_MaxVelocityChanged;
		}

		#endregion Constructor

		#region Scrolling Methods

		private void ScrollHelper_PositionChanged(object sender, EventArgs e)
		{
			try
			{
				double newPosition = _scrollHelper.Position;
				_textView.ViewScroller.ScrollViewportVerticallyByPixels(newPosition - _currentPosition);
				_currentPosition = newPosition;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("ScrollHelper_PositionChanged:  " + ex.Message);
			}
		}

		/// <summary>
		/// Handles the mouse down event before the default handler.
		/// </summary>
		/// <param name="e">The event arguments.</param>
		public override void PreprocessMouseDown(MouseButtonEventArgs e)
		{
			_scrollHelper.StopScrollAnimation();

			if (Settings.Instance.MiddleClickEnabled && e.ChangedButton == MouseButton.Middle)
			{
				try
				{
					double startPosition = e.GetPosition(_textView.VisualElement).Y;
					_scrollHelper.StartUserScroll(startPosition);

					_currentPosition = startPosition;

					Mouse.OverrideCursor = Cursors.Hand;
					e.Handled = true;
				}
				catch (Exception ex)
				{
					Trace.WriteLine("PreprocessMouseDown:  " + ex.Message);
				}
			}
		}

		/// <summary>
		/// Handles the mouse move event before the default handler.
		/// </summary>
		/// <param name="e">The event arguments.</param>
		public override void PreprocessMouseMove(MouseEventArgs e)
		{
			if (Settings.Instance.MiddleClickEnabled && Mouse.MiddleButton == MouseButtonState.Pressed)
			{
				try
				{
					double newPosition = e.GetPosition(_textView.VisualElement).Y;
					_scrollHelper.UpdateUserScroll(newPosition);

					e.Handled = true;
				}
				catch (Exception ex)
				{
					Trace.WriteLine("PreprocessMouseMove:  " + ex.Message);
				}
			}
		}

		/// <summary>
		/// Handles the mouse up event before the default handler.
		/// </summary>
		/// <param name="e">The event arguments.</param>
		public override void PreprocessMouseUp(MouseButtonEventArgs e)
		{
			if (Settings.Instance.MiddleClickEnabled && e.ChangedButton == MouseButton.Middle)
			{
				try
				{
					// TODO: Scroll to the nearest whole line?
					_scrollHelper.StopUserScroll();

					Mouse.OverrideCursor = null;
					e.Handled = true;
				}
				catch (Exception ex)
				{
					Trace.WriteLine("PreprocessMouseUp:  " + ex.Message);
				}
			}
		}

		/// <summary>
		/// Handles the mouse wheel event before the default handler.
		/// </summary>
		/// <param name="e">The event arguments.</param>
		public override void PreprocessMouseWheel(MouseWheelEventArgs e)
		{
			// no scrolling when ctrl is pressed (since that is used for zoom)
			if (Keyboard.Modifiers == ModifierKeys.Control)
				return;

			// Force kinetic mode with shift key.
			//// This does not work because VS does not call us when the shift keyis pressed?!?!?
			bool forceKinetic = false;//(Keyboard.Modifiers == ModifierKeys.Shift);

			try
			{
				// Delta is always 120, so we just divide by 120 to get plus or minus 1 for the direction
				int multiplier = e.Delta / 120;

				// We always want to land on the target depending on how far scroll wheel turned (like default VS behavior).
				if (!forceKinetic && Settings.Instance.ScrollSize > 0)
				{
					if (Keyboard.Modifiers == ModifierKeys.Shift)
					{
						var delta = _textView.ViewportHeight * multiplier;
						// when paging we don't want to go below the actual text.
						//var overshoot = _textView.ViewportBottom - (_textView.TextBuffer.CurrentSnapshot.LineCount * _textView.LineHeight - _textView.ViewportHeight);
						//if (overshoot > 0)
						//	delta = -overshoot;
                        _scrollHelper.UpdateSmoothScroll(delta);
					}
					else
						_scrollHelper.UpdateSmoothScroll(_textView.LineHeight * multiplier * Settings.Instance.ScrollSize);
                }
				// Change the velocity by the acceleration factor from settings
				else
				{
					_scrollHelper.StopScrollAnimation();
					_scrollHelper.ChangeVelocity(5 * multiplier * Settings.Instance.MouseWheelAccelerationFactor);
				}

				e.Handled = true;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("PreprocessMouseWheel:  " + ex.Message);
			}
		}

		#endregion Scrolling Methods
	}
}
