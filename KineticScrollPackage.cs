﻿using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Shell;

namespace KineticScroll
{
	/// <summary>
	/// This is the class that implements the package exposed by this assembly.
	/// </summary>
	/// <remarks>
	/// The minimum requirement for a class to be considered a valid package for Visual Studio
	/// is to implement the IVsPackage interface and register itself with the shell.
	/// This package uses the helper classes defined inside the Managed Package Framework (MPF)
	/// to do it: it derives from the Package class that provides the implementation of the
	/// IVsPackage interface and uses the registration attributes defined in the framework to
	/// register itself and its components with the shell.
	/// </remarks>
	// This attribute tells the PkgDef creation utility (CreatePkgDef.exe) that this class is a package
	[PackageRegistration(UseManagedResourcesOnly = true)]
	// This attribute is used to register the information needed to show this package in the Help/About dialog of Visual Studio
	[InstalledProductRegistration(PackageConstants.ProductNameResourceID, PackageConstants.ProductDetailsResourceID, PackageConstants.ProductVersion, IconResourceID = PackageConstants.ProductIconResourceID)]
	[ProvideOptionPage(typeof(OptionsPage), PackageConstants.OptionsPageCategoryResourceID, PackageConstants.OptionsPageNameResourceID, PackageConstants.OptionsPageCategoryResourceNumber, PackageConstants.OptionsPageNameResourceNumber, true)]
	[Guid(PackageConstants.PackageGuid)]
	public sealed class KineticScrollPackage : Package
	{
	    /// <summary>
		/// Initializes the package.
		/// </summary>
		/// This method is called right after the package is sited, so this is the place where
		/// you can put all the initialization code that rely on services provided by VisualStudio.
		/// </summary>
		protected override void Initialize()
		{
			base.Initialize();
		}
	}
}
