# Kinetic or Smooth Scroll for VS2013 & VS2015 #

Extension for Visual Studio 2013/15 that adds options to enable kinetic or smooth scrolling (middle mouse button and scroll wheel) in Visual Studio 2013/15 text editor.

**New for version 1.3:
**
Use setting SmoothScroll greater than 0 in order to enable smooth scrolling without kinetics. This gives you the same behavior as default VS scrolling except with smoothing.

Set up options my be found in `Tools -> Options -> Kinetic Scroll`.  It is also recommended to make sure hardware acceleration is enables as follows:

![vs-Options.jpg](https://bitbucket.org/repo/RKrG69/images/1587802790-vs-Options.jpg)

This project is based on and credits go to this original project: [http://vskineticscroll.codeplex.com/](http://vskineticscroll.codeplex.com/)

[**License**](http://vskineticscroll.codeplex.com/license)

### How to compile ###

* Install `Visual Studio 2013/15`
* Install `VS2013/15 SDK`
__________________________________________________________

###Contributions are encouraged!###

###TODO###
* reassemble using clean 2013 extension project from scratch *(now it is semi-hacked 2010 project with old `source.extension.vsixmanifest` file and probably some older dependencies versions)*

### Contacts ###

[parowoz@gmail.com](mailto:parowoz@gmail.com)
[philipg@audiophilleo.com](mailto:philipg@audiophilleo.com)