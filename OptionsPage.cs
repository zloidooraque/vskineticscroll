﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Shell;
using EnvDTE;

namespace KineticScroll
{
	/// <summary>
	/// The page that will be shown in Tools -> Options for the user to edit Kinetic Scroll settings.
	/// </summary>
	[Guid(PackageConstants.OptionsPageGuid)]
	[ComVisible(true)]
	[TypeConverter(typeof(PropertySorter))]
	[PropertyPageTypeConverter(typeof(PropertySorter))]
	public class OptionsPage : DialogPage
	{
		#region Constants

		private const string KineticScrollCategory = "Kinetic Scroll";

		#endregion Constants

		#region Variables

		private string _installedVersion;

		private bool _middleClickEnabled = Settings.MiddleClickEnabledDefaultValue;
		private int _scrollSize = Settings.ScrollSizeDefaultValue;

		private double _refreshRate = Settings.RefreshRateDefaultValue;
		private double _deceleration = Settings.DecelerationDefaultValue;
		private double _maxVelocity = Settings.MaxVelocityDefaultValue;

		private double _mouseWheelAccelerationFactor = Settings.MouseWheelAccelerationFactorDefaultValue;

		#endregion Variables

		#region Properties

		/// <summary>
		/// Gets or sets the installed version.
		/// </summary>
		/// <remarks>
		/// This doesn't get shown to the user but is used to save this setting to the Visual Studio environment properties.
		/// </remarks>
		/// <value>The installed version.</value>
		[Browsable(false)]
		public string InstalledVersion
		{
			get
			{
				return _installedVersion;
			}
			set
			{
				_installedVersion = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether [middle click enabled].
		/// </summary>
		/// <value><c>true</c> if [middle click enabled]; otherwise, <c>false</c>.</value>
		[Category(KineticScrollCategory)]
		[PropertyOrder(1)]
		[DefaultValue(Settings.MiddleClickEnabledDefaultValue)]
		[Description("Whether scrolling is enabled when clicking the middle mouse button and dragging or flicking")]
		public bool MiddleClickEnabled
		{
			get
			{
				return _middleClickEnabled;
			}
			set
			{
				_middleClickEnabled = value;
			}
		}

		/// <summary>
		/// Gets or sets the deceleration.
		/// </summary>
		/// <value>The deceleration.</value>
		[Category(KineticScrollCategory)]
		[PropertyOrder(2)]
		[DefaultValue(Settings.DecelerationDefaultValue)]
		[Description("The deceleration (in pixels per millisecond per millisecond) to apply to scrolling each interval.\nNot used if ScrollSize > 0.")]
		public double Deceleration
		{
			get
			{
				return _deceleration;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentException("Deceleration must not be less than 0");
				}

				_deceleration = value;
			}
		}

		/// <summary>
		/// Gets or sets the max velocity.
		/// </summary>
		/// <value>The max velocity.</value>
		[Category(KineticScrollCategory)]
		[PropertyOrder(3)]
		[DefaultValue(Settings.MaxVelocityDefaultValue)]
		[Description("The maximum scrolling velocity (in pixels per millisecond).\nNot used if ScrollSize > 0.")]
		public double MaxVelocity
		{
			get
			{
				return _maxVelocity;
			}
			set
			{
				if (value <= 0)
				{
					throw new ArgumentException("MaxVelocity must be > 0");
				}

				_maxVelocity = value;
			}
		}

		/// <summary>
		/// Gets or sets the mouse wheel acceleration factor.
		/// </summary>
		/// <value>The mouse wheel acceleration factor.</value>
		[Category(KineticScrollCategory)]
		[PropertyOrder(4)]
		[DefaultValue(Settings.MouseWheelAccelerationFactorDefaultValue)]
		[Description("The acceleration factor to apply to velocity each time a mouse wheel event is raised.\nNot used if ScrollSize > 0.")]
		public double MouseWheelAccelerationFactor
		{
			get
			{
				return _mouseWheelAccelerationFactor;
			}
			set
			{
				if (value < 1)
				{
					throw new ArgumentException("MouseWheelAccelerationFactor must be greater or equal than 1");
				}

				_mouseWheelAccelerationFactor = value;
			}
		}

		/// <summary>
		/// Gets or sets the refresh interval.
		/// </summary>
		/// <value>The refresh interval.</value>
		[Category(KineticScrollCategory)]
		[PropertyOrder(5)]
		[DefaultValue(Settings.RefreshRateDefaultValue)]
		[Description("The scroll frame rate (use 2x your monitor refresh rate. E.g. 120)")]
		public double RefreshRate
		{
			get
			{
				return _refreshRate;
			}
			set
			{
				if (value <= 10)
				{
					throw new ArgumentException("RefreshRate must be greater than 10");
				}

				_refreshRate = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether we only smooth scrolling instead of Kinetic.
		/// </summary>
		/// <value><c>true</c> if [mouse wheel enabled]; otherwise, <c>false</c>.</value>
		[Category(KineticScrollCategory)]
		[PropertyOrder(6)]
		[DefaultValue(Settings.ScrollSizeDefaultValue)]
		[Description("If 0, kinetic scrolling is used.\nIf > 0, smoothed scrolling is used and the original VS scroll behavior is kept (scrolling stops instantly when the mousewheel stops).\nRefreshRate is the only other setting that applies.\nE.g. '3' scrolls three lines per mouse wheel tick.")]
		public int ScrollSize
		{
			get
			{
				return _scrollSize;
			}
			set
			{
				_scrollSize = value;
			}
		}

		#endregion Properties

		#region Event Handlers

		/// <summary>
		/// Handles Apply messages from the Visual Studio environment.
		/// </summary>
		/// <devdoc>
		/// This method is called when VS wants to save the user's changes then the dialog is dismissed.
		/// </devdoc>
		protected override void OnApply(PageApplyEventArgs e)
		{
			base.OnApply(e);

			if (e.ApplyBehavior == ApplyKind.Apply)
			{
				Settings.Instance.MiddleClickEnabled = _middleClickEnabled;
				Settings.Instance.ScrollSize = _scrollSize;
				Settings.Instance.RefreshRate = _refreshRate;
				Settings.Instance.Deceleration = _deceleration;
				Settings.Instance.MaxVelocity = _maxVelocity;
				Settings.Instance.MouseWheelAccelerationFactor = _mouseWheelAccelerationFactor;
			}
		}

		#endregion Event Handlers
	}
}